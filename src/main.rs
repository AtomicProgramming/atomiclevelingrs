use std::path::Path;
use std::error::Error;
use std::process::exit;
use serde::{Serialize, Deserialize};
use std::fs::File;
use std::io::{Read, Write};
use serenity::{prelude::*, model::{channel::Message, gateway::Ready}, async_trait, Client};

#[derive(Serialize, Deserialize)]
struct Config {
    token: Option<String>,
}

fn load_config() -> Result<Config, Box<dyn Error>> {
    let mut config = vec!{};
    File::open("config.toml")?.read_to_end(&mut config)?;
    Result::Ok(toml::from_slice(&config)?)
}

fn generate_config() -> Result<(), Box<dyn Error>> {
    let default_config =
    "token = ''  # your discord token here";
    File::create("config.toml")?.write_all((default_config).as_ref())?;
    Ok(())
}

struct Handler;

#[async_trait]
impl EventHandler for Handler {

    async fn message(&self, ctx: Context, msg: Message) {
        if msg.content == "hey" {
            if let Err(why) = msg.channel_id.say(&ctx.http, "Hello there").await {
                println!("Could not send message, {:?}", why);
            }
        }
    }

    async fn ready(&self, _: Context, ready: Ready) {
        println!("Connected to discord as the user '{}'", ready.user.tag());
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
   // check if config toml exists
   let config_exists = Path::new("config.toml").is_file();
    if !config_exists {
        println!("Could not find config, generating one for you, exiting...");
        generate_config()?;
        exit(1);
    }

    let config: Config = load_config()?;
    if config.token.is_none() {
        println!("No token defined in config, exiting...");
        exit(1);
    }
    let token = config.token.unwrap();
    let mut client = Client::builder(&token).event_handler(Handler).await.expect("Failed to create client");
    if let Err(why) = client.start().await {
        println!("Failed to start client, {:?}", why);
    }
    Ok(())
}
